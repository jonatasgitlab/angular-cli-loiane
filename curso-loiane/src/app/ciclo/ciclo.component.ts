import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-ciclo',
  templateUrl: './ciclo.component.html',
  styleUrls: ['./ciclo.component.css'],
})
export class CicloComponent implements OnInit {
  @Input()
  valorInicial = 10;

  constructor() {
    console.log('CONSTRUTOR');
  }

  ngOnChanges() {
    console.log('ON CHANGES');
  }

  ngOnInit(): void {
    console.log('ON INIT');
  }

  ngDoCheck() {
    console.log('DO CHECK');
  }

  ngAfterContentInit() {
    console.log('AFTER CONTENT INIT');
  }

  ngAfterContentChecked() {
    console.log('AFTER CONTENT CHECKED');
  }

  ngAfterViewInit() {
    console.log('AFTER VIEW INIT');
  }

  ngAfterViewChecked() {
    console.log('AFTER VIEW CHECKED');
  }

  ngOnDestroy(){
    console.log('ON DESTROY');
    
  }
}
