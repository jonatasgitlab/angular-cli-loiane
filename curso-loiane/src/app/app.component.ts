import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'curso-loiane';

  valor=5;
  cicloExists: boolean = true;

  mudarValor(){
    this.valor++;
  }

  deletar(){
    this.cicloExists  = !this.cicloExists;
  }
}
