import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.scss'],
})
export class DataBindingComponent implements OnInit {
  interpolation: string = 'Resultado Interpolação';
  urlImage = 'https://picsum.photos/200/300';
  valorAtual: string = "";
  isMouseOver: boolean = false;
  nomeDoCurso = 'Angular';

  valorInicial = 15;

  constructor() {}

  ngOnInit(): void {}

  getValor() {
    return 99;
  }
  btnClick(){
    console.log('alo');
    
  }

  onKeyUp(evento: KeyboardEvent){
  
    this.valorAtual = (<HTMLInputElement>evento.target).value;
  }

  salvarValor(valorInput){
    console.log(valorInput);
    
  }

  mouseOverOut(){
    this.isMouseOver = !this.isMouseOver;
  }

  onMudouValor(evento){
    console.log(evento.novoValor);
    
  }
}
