import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import * as M from "materialize-css/dist/js/materialize";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  data: any = [
    {
      "conta": "XYZ",
      "quantidade": "10",
      "total": "25000",
      "datas": [
        {
          "dia":"01/10/2021",
          "quantidade": "10",
          "total": "25000",
          "tipoPagto": [
            {
              "nome" : "TED",
              "quantidade": "5",
               "total": "10000"
            },
            {
              "nome" : "BOLETO",
              "quantidade": "5",
               "total": "15000"
            }
          ]
        },
        {
          "dia":"02/10/2021",
          "quantidade": "20",
          "total": "25000",
          "tipoPagto": [
            {
              "nome" : "TED",
              "quantidade": "15",
               "total": "10000"
            },
            {
              "nome" : "BOLETO",
              "quantidade": "5",
               "total": "15000"
            }
          ]
        }
      ]
    },
    {
      "conta": "qwe",
      "quantidade": "10",
      "total": "25000",
      "datas": [
        {
          "dia":"01/10/2021",
          "quantidade": "10",
          "total": "25000",
          "tipoPagto": [
            {
              "nome" : "TED",
              "quantidade": "5",
               "total": "10000"
            },
            {
              "nome" : "BOLETO",
              "quantidade": "5",
               "total": "15000"
            }
          ]
        },
        {
          "dia":"02/10/2021",
          "quantidade": "20",
          "total": "25000",
          "tipoPagto": [
            {
              "nome" : "TED",
              "quantidade": "15",
               "total": "10000"
            },
            {
              "nome" : "BOLETO",
              "quantidade": "5",
               "total": "15000"
            }
          ]
        }
      ]
    }
  ]

  @ViewChild('header') el: ElementRef;
  
  ngOnInit(): void {
    $(document).ready(function() {
      $('tr:not(.header)').hide();
    
      $('tr.header').click(function() {
        $(this).find('span').text(function(_, value) {
          return value == '-' ? '+' : '-'
        });
        
        $(this).nextUntil('tr.header').slideToggle(100, function() {});
      });
    });
    
  }
  title = 'app';
  jQuery: any;
}
